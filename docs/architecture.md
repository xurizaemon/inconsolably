# Architecture

## Plugin architecture

From [docs/journal/2019-02-24.md](/docs/journal/2019-02-24.md):

A plugin is:

- Capable of setting up authentication via Gitlab. (Could be a form to input a token, or an OAuth2 setup, or ...).
- Can retrieve data.
- May have multiple datastreams (eg tweets, likes, retweets, follows).
- Can store data. (This isn't always necessary or desirable?)
- Can display data.
- Can work with multiple instances of that source.
- Can perform actions on retrieved data.
  - Modify before storage.
  - Trigger responses to data.
- Can be used creatively.
  - Expressive use is important!
