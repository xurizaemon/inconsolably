# Inconsolably - 2019-02-24 08:30AM

Actually don't like this project name because it suggests at a .ly domain, which is Libya, which has a pretty uncool govt. Maybe "chrissole" cos it's a console for Chris. Much better! Anyway, naming is hard. So let's just tag that and move on.

## What is it

I want a **console to review things**. Basically I want a **personal dashboard**. I want a **start page** (maybe this project should be [Chris's Lynx Links 2.0](https://web.archive.org/web/19990125090628/http://linux.ibex.co.nz/)). I want it to have nice buttons for my essential bookmarks so it's a good browser start page. I also want it to retrieve a bunch of data and aggregate it and make a personal lifestream for my consumption.

This might get a bit overloading.

## Design

I want it to be plugin based, so you can add new functionality. Here are some plugin ideas:

* Social media feeds
* Personal health
* Billable hours logged
* Commits
* RSS (a generally available source)
* Pinboard
* Xero
* Browser bookmarks / links

I've got a longer list of items somewhere but that's a glimpse.

I want it to be efficient to consume. One of my reasons for wanting this tool is that I often look back at my history across a range of sources for something - I might be looking for a followup task after a meeting, or I might be trying to reconstruct a day's activities to log billable hours. So knowing my search history, or URLs browsed, or location, would be very useful in one spot.

## Data ownership

This is extremely stalky, so it's important to me that this project be a thing people can run themselves easily. Well, I don't want to end up delivering it as a service - unless that can be done safely, and probably safer to not. A browser extension might be a good solution, since the data can be located with the user there. Cross-browser sync might be an option if it can be securely shared between installations and browsers. Any hosted / aaS version would need to be thoroughly encrypted for storage.

## How it looks

I want to have a timeline view. A compact timeline view looks a bit like this:

![compact timeline sketch](/docs/journal/media/IMG_20190224_083715__01-compact_timeline_sketch.jpg)

I want it to have a dashboard view. This might embed the above (in some displays). It might look like this:

![dashboard header sketch](/docs/journal/media/IMG_20190224_083752__01-compact_dashboard_sketch.jpg)

A nice idea about some of these graphs is that, since it's such a personal project, many of them won't need labelling. Eg I can view my own weight (lowest weigh-in in several years yesterday!) without labelling it loudly.

## Plugin architecture

A plugin is:

- Capable of setting up authentication via Gitlab. (Could be a form to input a token, or an OAuth2 setup, or ...).
- Can retrieve data.
- Can store data. (This isn't always necessary or desirable?)
- Can display data.
- Can work with multiple instances of that source.
- Can perform actions on retrieved data.
  - Modify before storage.
  - Trigger responses to data.
- Can be used creatively.
  - Expressive use is important!
