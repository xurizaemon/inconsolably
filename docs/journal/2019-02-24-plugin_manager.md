# 2019-02-24 10:11 AM – Inconsolably (now CLL2?)

OK, first question is how to build the plugin architecture to achieve what I want.

A plugin should be able to:

- Fetch data from a feed
- Display elements

First stab of plugins is that they are React elements, which might make sense, but maybe they should be a package which provides a React element and other items. (Don't want to lock too tightly to React, and that might not be the right layout.)

So I guess I need:

- A pluginmanager which can scan for packages that provide a plugin type, OR
- A pluginmanager which can scan a directory.
- An event system ([Observer or Publish/Susbcribe](https://addyosmani.com/resources/essentialjsdesignpatterns/book/#observerpatternjavascript)) to notify of events.

Some reading:

* [SO: Plugin Architecture in Web Apps (Examples or Code Snippets?)](https://stackoverflow.com/questions/10763006/plugin-architecture-in-web-apps-examples-or-code-snippets)
* [Building Plugins for React Apps](https://www.nylas.com/blog/react-plugins/)
* [Atom's package manager](https://github.com/atom/atom/blob/133d9153ede248529204b5bfd6dff5bbc02eba69/src/package-manager.js) and [building your first Atom plugin](https://github.blog/2016-08-19-building-your-first-atom-plugin/).
* [Gulp plugins](https://gulpjs.com/docs/en/getting-started/using-plugins)
* [VS Code's extension manager](https://github.com/Microsoft/vscode/blob/b4445a9f736a3a4d235391d9ff99d304acecd25e/build/lib/extensions.js).
* [Yeoman generator installer](https://github.com/yeoman/yo/blob/ae35426ba4c11001827ac9d8e281bb64f5770271/lib/routes/install.js) and [Yeoman's Generators](https://yeoman.io/authoring/)
* [plugin-manager](https://www.npmjs.com/package/plugin-manager)
* [polite-plugin-manager](https://www.npmjs.com/package/polite-plugin-manager)
* [passport-strategy](https://github.com/jaredhanson/passport-strategy)

### Atom

Atom's package manager looks like it reads the `engines` property of the package's details from `package.json`, and uses that to determine if it's looking at an Atom package.

Atom's package manager has a reasonable layout for plugin lifecycle (install/uninstall/activate/deactivate etc), has code for things like reset and deprecated packages, and I know it has `apm` to demonstrate install/uninstall etc too.

Does it support arbitrary hooks, or do you need to manually add each hook to the `package-manager.js` file?

### VS Code

It's Sunday, I'm tired and I can't read that code.

### Yo

Yo looks for NPM packages (from NPM and on filesystem) which start with `generator-` prefix. I don't know that it particularly does event-like things; it does support dynamic plugin retrieval and use but may lack other features I want.

### Gulp

Gulp is interesting, it has a pretty simple plugin architecture. Seems like plugins there are a specific type of object (a Node Transform Stream).

> Hi Chris. Personally I would follow Express' use of middlewares, or Passport strategies as a very basic template. In other words, you'd have your main app and let's say an array of plugins. Your app calls them one by one, possibly in async fashion, and you wait for them to return data, or call each with a next() function they should call when done processing.  The data that each plugin returns must follow a consistent structure, which your app gets to impose and you detail in the documentation. hope this helps as a starting point

- Alex D in https://wesbos.slack.com/archives/C0B6V0SJ3/p1550992233050200?thread_ts=1550956210.047700&cid=C0B6V0SJ3
