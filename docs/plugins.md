# Plugins

As I type this, zero plugins exist.

## List of plugin ideas

* Atom / RSS
* Browser bookmarks
* Browser history
* FitBit
* Github
* Gitlab
* Journal
* Mastodon
* MyFitnessPal
* Stack Exchange
* Toggl
* Twitter
* WRMS (Timesheet, Updates)
* Xero
* Yunmai
