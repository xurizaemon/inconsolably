import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <ul className="timeline">
            <li className="timeline-entry timeline-entry-yunmai-weight">Weighed 84.9kg</li>
            <li className="timeline-entry timeline-entry-twitter-retweet">RT'd 2 tweets</li>
            <li className="timeline-entry timeline-entry-wrms-timesheet">6.5H on 2 tasks</li>
            <li className="timeline-entry timeline-entry-gitlab-activity">4 commits, 2 MRs</li>
            <li className="timeline-entry timeline-entry-pinboard-bookmarks">2 saved links</li>
          </ul>
        </header>
      </div>
    );
  }
}

export default App;
