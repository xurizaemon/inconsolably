import React, { Component } from 'react';
import './DemoPluginTwitter.css';

class DemoPluginTwitter extends Component {
  render() {
    return (
      <li className="timeline-entry timeline-entry-twitter-retweet">RT'd 2 tweets</li>
    );
  }
}

export default DemoPluginTwitter;
