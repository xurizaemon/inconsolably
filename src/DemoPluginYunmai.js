import React, { Component } from 'react';
import './DemoPluginYunmai.css';

class DemoPluginYunmai extends Component {
  render() {
    return (
      <li className="timeline-entry timeline-entry-yunmai-weight">Weighed 84.9kg</li>
    );
  }
}

export default DemoPluginYunmai;
