import React, { Component } from 'react';
import logo from './logo.svg';
import './DemoTimeline.css';
import DemoPluginYunmai from './DemoPluginYunmai';
import DemoPluginTwitter from './DemoPluginTwitter';

class DemoTimeline extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <ul className="timeline">
            <DemoPluginYunmai />
            <DemoPluginTwitter />
            <li className="timeline-entry timeline-entry-gitlab-activity">4 commits, 2 MRs</li>
            <li className="timeline-entry timeline-entry-pinboard-bookmarks">2 saved links</li>
          </ul>
        </header>
      </div>
    );
  }
}

export default DemoTimeline;
